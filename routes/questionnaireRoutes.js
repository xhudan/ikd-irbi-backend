const router = require("express").Router();

const {
    getQuestionnaires,
    addQuestionnaire,
    updateQuestionnaire,
    deleteQuestionnaire,
    getVerifFile
} = require("../controllers/questionnaireController.js");

// middleware
const Auth = require("../middleware/auth.js");
const upload = require("../middleware/upload.js");

// API
router.get("/downloads/:filename", getVerifFile);
router.get("/api/v1/*/:priorityId/:indicatorId", Auth.verifyUser, getQuestionnaires);
router.post("/api/v1/*/*/:indicatorId", Auth.verifyUser, Auth.isAdmin, addQuestionnaire);
router.put("/api/v1/*/*/:indicatorId/:questionnaireId", Auth.verifyUser, upload, updateQuestionnaire);
router.delete("/api/v1/*/*/:indicatorId/:questionnaireId", Auth.verifyUser, Auth.isAdmin, deleteQuestionnaire);


module.exports = router;
