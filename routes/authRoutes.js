const router = require("express").Router();

const {
  login,
  register,
  getAllUsers,
  getUser,
  updateUser
} = require("../controllers/authControllers.js");

// middleware
const Auth = require("../middleware/auth.js");

// for get all users debugging
router.get("/users", getAllUsers);

// API
router.post("/login", login);
router.post("/register", Auth.verifyUser, Auth.isAdmin, register);
router.get("/user/:id", Auth.verifyUser, Auth.isAdmin, getUser);
router.put("/update-user/:id", Auth.verifyUser, Auth.isAdmin, updateUser);

module.exports = router;
