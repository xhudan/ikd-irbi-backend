const router = require("express").Router();

const {
    getIndicators,
    addIndicator,
    updateIndicator,
    deleteIndicator
} = require("../controllers/indicatorControllers.js");

// middleware
const Auth = require("../middleware/auth.js");

// API
router.get("/*/:priorityId", Auth.verifyUser, getIndicators);
router.post("/*/:priorityId", Auth.verifyUser, Auth.isAdmin, addIndicator);
router.put("/*/:priorityId/:indicatorId", Auth.verifyUser, Auth.isAdmin, updateIndicator);
router.delete("/*/:priorityId/:indicatorId", Auth.verifyUser, Auth.isAdmin, deleteIndicator);

module.exports = router;
