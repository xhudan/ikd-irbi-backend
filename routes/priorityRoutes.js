const router = require("express").Router();

const {
    filterByYear,
    getAllDataByYear,
    getPriorities,
    addPriority,
    bulkCreateData,
    updatePriority,
    deletePriority
} = require("../controllers/priorityControllers.js");

// middleware
const Auth = require("../middleware/auth.js");

// API
router.post("", Auth.verifyUser, Auth.isAdmin, addPriority);
router.post("/bulkcreate", Auth.verifyUser, Auth.isAdmin, bulkCreateData);
router.get("", Auth.verifyUser, getPriorities);
router.get("/export", Auth.verifyUser, Auth.isAdmin, getAllDataByYear);
router.get("/:y", Auth.verifyUser, filterByYear);
router.put("/*/:priorityId", Auth.verifyUser, Auth.isAdmin, updatePriority);
router.delete("/*/:priorityId", Auth.verifyUser, Auth.isAdmin, deletePriority);

module.exports = router;
