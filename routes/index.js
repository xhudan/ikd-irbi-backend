const router = require("express").Router();
// const swaggerUi = require("swagger-ui-express");
// const swaggerDocument = require("../docs/swagger.json");
const authRoutes = require("./authRoutes");
const priorityRoutes = require("./priorityRoutes");
const indicatorRoutes = require("./indicatorRoutes");
const questionnaireRoutes = require("./questionnaireRoutes");

// //Open API
// router.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// // API
router.use("", questionnaireRoutes);
router.use("/api/v1/auth", authRoutes);
router.use("/api/v1", indicatorRoutes);
router.use("/api/v1", priorityRoutes);

module.exports = router;
