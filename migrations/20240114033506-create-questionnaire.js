'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('questionnaires', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      number_of_questionnaire: {
        type: Sequelize.INTEGER
      },
      fill_in_questionnaire: {
        type: Sequelize.TEXT
      },
      answer: {
        type: Sequelize.ENUM('Ya', 'Tidak')
      },
      verification: {
        type: Sequelize.TEXT
      },
      upload_verification: {
        type: Sequelize.TEXT
      },
      indicator_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('questionnaires');
  }
};