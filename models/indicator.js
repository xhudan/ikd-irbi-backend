'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class indicator extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.priority, { foreignKey: "priority_id" });
      this.hasMany(models.questionnaire, { foreignKey: "indicator_id" });
    }
  }
  indicator.init({
    indicator_name: DataTypes.STRING,
    description: DataTypes.TEXT,
    priority_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'indicator',
  });
  return indicator;
};