'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class questionnaire extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.user, { foreignKey: "user_id" });
      this.belongsTo(models.indicator, { foreignKey: "indicator_id" });
    }
  }
  questionnaire.init({
    number_of_questionnaire: DataTypes.INTEGER,
    fill_in_questionnaire: DataTypes.TEXT,
    answer: DataTypes.ENUM('Ya', 'Tidak'),
    verification: DataTypes.TEXT,
    upload_verification: DataTypes.TEXT,
    indicator_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'questionnaire',
  });
  return questionnaire;
};