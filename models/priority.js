'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class priority extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.indicator, { foreignKey: "priority_id" });
    }
  }
  priority.init({
    priority_name: DataTypes.STRING,
    description: DataTypes.TEXT,
    year: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'priority',
  });
  return priority;
};