const multer = require("multer");

const getFileExtension = (filename) => {
  return filename.split('.').pop();
};

const fileFilter = (req, file, cb) => {
  const allowedMimeTypes = ["application/pdf", "image/png", "image/jpg", "image/jpeg"];

  if (allowedMimeTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(new Error('Invalid file type'), false);
  }
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/');
  },
  filename: (req, file, cb) => {
    const { questionnaireId } = req.params;

    if (!questionnaireId) {
      cb(new Error('Missing questionnaireId in request parameters'), null);
      return;
    }

    const filetype = getFileExtension(file.originalname);
    const currentDate = new Date().toISOString().slice(0, 10).replace(/-/g, ''); // Format: YYYYMMDD
    const filename = `${questionnaireId}.${currentDate}.${filetype}`;

    cb(null, filename);
  }
});

const upload = multer({
  fileFilter: fileFilter,
  storage: storage
}).single('upload_verification');

module.exports = upload;
