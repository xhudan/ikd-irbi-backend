const { priority, indicator, questionnaire } = require("../models");
const catchAsync = require("../utils/catchAsync");
const DATA = require("../lib/data");

const filterByYear = catchAsync(async (req, res) => {
  const { y } = req.params;
  await priority
    .findAll({ where: { year: y } })
    .then((data) => {
      if (data.length === 0) {
        res.status(404).json({ msg: 'Data is empty' });
      } else {
        res.status(200).json({ data });
      }
    })
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const getAllDataByYear = catchAsync(async (req, res) => {
  const { y } = req.query;
  await priority
    .findAll({
      where: { year: y }, include: [{
        model: indicator, include: [{
          model: questionnaire
        }]
      }]
    })
    .then((data) => res.status(200).json({ data }))
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const getPriorities = catchAsync(async (req, res) => {
  await priority
    .findAll()
    .then((data) => res.status(200).json({ data }))
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const addPriority = catchAsync(async (req, res) => {
  const { priority_name, year } = req.body;

  await priority
    .create({ priority_name, year })
    .then((data) => res.status(201).json({ msg: "Priority created successfully", data }))
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const bulkCreateData = catchAsync(async (req, res) => {
  const { year } = req.body;

  // Validate year input
  if (!year || isNaN(year)) {
    return res.status(400).json({ msg: 'Invalid year input' });
  }

  const createdPriorities = await priority.bulkCreate(
    DATA.priority.map((p) => ({ ...p, year }))
  );

  // Define indicator ranges for each priority
  const priorityRanges = [
    { start: 1, end: 9 },
    { start: 10, end: 13 },
    { start: 14, end: 26 },
    { start: 27, end: 31 },
    { start: 32, end: 43 },
    { start: 44, end: 67 },
    { start: 68, end: 71 },
  ];

  // Create indicators and associate with priorities
  const createdIndicators = [];
  let priorityIndex = 0;  // Initialize priority index

  for (const range of priorityRanges) {
    // Extract indicators based on the specified range
    const indicatorsData = DATA.indicator
      .slice(range.start - 1, range.end)
      .map((indicator) => ({
        ...indicator,
        priority_id: createdPriorities[priorityIndex].id,
      }));

    // Bulk create indicators and store the result
    const created = await indicator.bulkCreate(indicatorsData);
    createdIndicators.push(...created);

    // Increment priority index for the next set of indicators
    priorityIndex++;
  }

  // Reset priority index for the next set of indicators
  priorityIndex = 0;

  // Create questionnaires and associate with indicators
  const questionnairesData = createdIndicators.flatMap((indicator, index) => {
    const startIndex = index * 4;
    const endIndex = startIndex + 4;
    const limitedQuestionnaires = DATA.questionnaire.slice(startIndex, endIndex);

    return limitedQuestionnaires.map((question) => ({
      ...question,
      indicator_id: indicator.id,
    }));
  });

  await questionnaire.bulkCreate(questionnairesData);

  return res.status(200).json({ msg: 'Questionnaires added successfully' });
});

const updatePriority = catchAsync(async (req, res) => {
  const { priorityId } = req.params;
  const { priority_name, year } = req.body;

  await priority
    .update({ priority_name, year }, { where: { id: priorityId } })
    .then(() => res.status(201).json({ msg: "Priority updated successfully" }))
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const deletePriority = catchAsync(async (req, res) => {
  const { priorityId } = req.params;

  try {
    // Get all indicators for the given priority
    const indicators = await indicator.findAll({ where: { priority_id: priorityId } });

    // Extract indicator ids
    const indicatorIds = indicators.map(indicator => indicator.id);

    // Delete questionnaires associated with the indicators
    const questionnaireResult = await questionnaire.destroy({ where: { indicator_id: indicatorIds } });

    // Delete indicators
    const indicatorResult = await indicator.destroy({ where: { priority_id: priorityId } });

    // Delete priority
    const priorityResult = await priority.destroy({ where: { id: priorityId } });

    if (questionnaireResult && indicatorResult && priorityResult) {
      res.status(200).json({ msg: "Data deleted successfully" });
    } else if (priorityResult) {
      res.status(200).json({ msg: "Priority deleted successfully" });
    } else {
      res.status(404).json({ msg: "Data not found" });
    }
  } catch (err) {
    res.status(err.statusCode || 500).json({ msg: err.message });
  }
});

module.exports = {
  filterByYear,
  getAllDataByYear,
  getPriorities,
  addPriority,
  bulkCreateData,
  updatePriority,
  deletePriority
};
