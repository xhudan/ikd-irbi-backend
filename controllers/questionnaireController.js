const { indicator, questionnaire } = require("../models");
const catchAsync = require("../utils/catchAsync");
const fs = require('fs');

const getQuestionnaires = catchAsync(async (req, res) => {
    const { priorityId, indicatorId } = req.params;
    await indicator
        .findOne({ where: { priority_id: priorityId, id: indicatorId }, include: [questionnaire] })
        .then((data) => {
            if (!data || !data.questionnaires || data.questionnaires.length === 0) {
                res.status(404).json({ msg: 'Questionnaires not found!' });
            } else {
                res.status(200).json({ data });
            }
        })
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const addQuestionnaire = catchAsync(async (req, res) => {
    const { indicatorId } = req.params;
    const { number_of_questionnaire, fill_in_questionnaire } = req.body;

    await questionnaire
        .create({ indicator_id: indicatorId, user_id: req.user.id, number_of_questionnaire, fill_in_questionnaire })
        .then((data) => res.status(200).json({ msg: "Questionnaire created successfully", data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const updateQuestionnaire = catchAsync(async (req, res) => {
    const { indicatorId, questionnaireId } = req.params;
    const { number_of_questionnaire, fill_in_questionnaire, answer, verification } = req.body;

    let updateFields = {
        user_id: req.user.id,
        number_of_questionnaire,
        fill_in_questionnaire,
        answer,
        verification,
    };

    if (req.file) {
        const fileURL = `${req.protocol}://${req.get('host')}/downloads/${req.file.filename}`;
        updateFields.upload_verification = fileURL;
    }

    await questionnaire
        .update(updateFields, { where: { indicator_id: indicatorId, id: questionnaireId } })
        .then(() => res.status(200).json({ msg: "Questionnaire updated successfully" }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const getVerifFile = catchAsync(async (req, res) => {
    const { filename } = req.params;
    const filePath = `uploads/${filename}`;

    // Check if the file exists
    if (!fs.existsSync(filePath)) {
        return res.status(404).json({ msg: 'File not found' });
    }

    res.download(filePath, filename, (err) => {
        if (err) {
            console.error(err);
            res.status(500).json({ msg: 'Internal Server Error' });
        }
    });
});

const deleteQuestionnaire = catchAsync(async (req, res) => {
    const { indicatorId, questionnaireId } = req.params;
    await questionnaire
        .destroy({ where: { indicator_id: indicatorId, id: questionnaireId } })
        .then((data) => res.status(200).json({ msg: "Questionnaire deleted successfully", data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

module.exports = {
    getQuestionnaires,
    addQuestionnaire,
    updateQuestionnaire,
    deleteQuestionnaire,
    getVerifFile
};
