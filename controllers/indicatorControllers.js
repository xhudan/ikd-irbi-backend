const { indicator, questionnaire } = require("../models");
const catchAsync = require("../utils/catchAsync");

const getIndicators = catchAsync(async (req, res) => {
  const { priorityId } = req.params;
  await indicator
    .findAll({ where: { priority_id: priorityId } })
    .then((data) => {
        if (data.length === 0) {
          res.status(404).json({ msg: 'Data is empty' });
        } else {
          res.status(200).json({ data });
        }
      })
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const addIndicator = catchAsync(async (req, res) => {
    const { priorityId } = req.params;
    const { indicator_name, description } = req.body;

    await indicator
        .create({ indicator_name, description, priority_id: priorityId })
        .then((data) => res.status(200).json({ msg: "Indicator created successfully", data }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const updateIndicator = catchAsync(async (req, res) => {
    const { priorityId, indicatorId } = req.params;
    const { indicator_name, description } = req.body;

    await indicator
        .update({ indicator_name, description }, { where: { priority_id: priorityId, id: indicatorId } })
        .then(() => res.status(200).json({ msg: "Indicator updated successfully" }))
        .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

const deleteIndicator = catchAsync(async (req, res) => {
    const { priorityId, indicatorId } = req.params;

    try {
        const questionnaireResult = await questionnaire.destroy({ where: { indicator_id: indicatorId } });
        const indicatorResult = await indicator.destroy({ where: { priority_id: priorityId, id: indicatorId } });

        if (questionnaireResult && indicatorResult) {
            res.status(200).json({ msg: "Data deleted successfully" });
        } else if (indicatorResult) {
            res.status(200).json({ msg: "Indicator deleted successfully" });
        } else {
            res.status(404).json({ msg: "Data not found" });
        }
    } catch (err) {
        res.status(err.statusCode || 500).json({ msg: err.message });
    }
});

module.exports = {
    getIndicators,
    addIndicator,
    updateIndicator,
    deleteIndicator
};
