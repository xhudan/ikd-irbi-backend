const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const catchAsync = require("../utils/catchAsync");
const { user } = require("../models");

// user login
const login = catchAsync(async (req, res) => {
  const { username, password } = req.body;

  await user
    .findOne({ where: { username } })
    .then(async (user) => {
      if (!user) return res.status(400).json({ msg: "Sorry, your username account doesn't exist." });

      const validPassword = await bcrypt.compare(password, user.password);
      if (!validPassword) return res.status(400).json({ msg: "Passwords don't match" });

      const token = jwt.sign({ id: user.id, role: user.role }, process.env.SECRET_KEY);
      res.status(200).json({ msg: "Login successfully", token, user });
    })
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

// user register
const register = catchAsync(async (req, res) => {
  const { fullname, username, password, role } = req.body;

  if (fullname === "" || username === "" || password === "" || role === "") {
    return res.status(400).json({ msg: "Please input a relevant data" });
  } else {
    const User = await user.findOne({ where: { username } });
    if (User) return res.status(400).json({ msg: "Username already exists" });

    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = await user
      .create({
        fullname,
        username,
        password: hashedPassword,
        role
      })
      .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
  }
});

// test users
const getAllUsers = catchAsync(async (req, res) => {
  await user
    .findAll()
    .then((user) => res.status(200).json({ user }))
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

// Get User
const getUser = catchAsync(async (req, res) => {
  const { id } = req.user;
  await user
    .findOne({ where: { id } })
    .then((user) => res.status(200).json({ user }))
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

// Update User
const updateUser = catchAsync(async (req, res) => {
  const { id } = req.user;
  const { fullname, username, password } = req.body;

  await user
    .update({ fullname, username, password }, { where: { id } })
    .then(() => res.status(201).json({ msg: "User updated successfully" }))
    .catch((err) => res.status(err.statusCode || 500).json({ msg: err.message }));
});

module.exports = {
  login,
  register,
  getAllUsers,
  updateUser,
  getUser
};
