const DATA = {
	priority: [
		{
			priority_name: "Priority 1",
			description: "PERKUATAN KEBIJAKAN DAN KELEMBAGAAN",
		},

		{
			priority_name: "Priority 2",
			description: "PENGKAJIAN RISIKO DAN PERENCANAAN TERPADU",
		},

		{
			priority_name: "Priority 3",
			description: "PENGEMBANGAN SISTEM INFORMASI, DIKLAT DAN LOGISTIK",
		},

		{
			priority_name: "Priority 4",
			description: "PENANGANAN TEMATIK KAWASAN RAWAN BENCANA",
		},

		{
			priority_name: "Priority 5",
			description: "PENINGKATAN EFEKTIVITAS PENCEGAHAN DAN MITIGASI BENCANA",
		},

		{
			priority_name: "Priority 6",
			description: "PERKUATAN KESIAPSIAGAAN DAN PENANGANAN DARURAT BENCANA",
		},

		{
			priority_name: "Priority 7",
			description: "PENGEMBANGAN SISTEM PEMULIHAN BENCANA",
		},
	],

	indicator: [
		{
			indicator_name: "Indicator 1",
			description: "Peraturan Daerah tentang Penyelenggaraan PB",
		},

		{
			indicator_name: "Indicator 2",
			description: "Peraturan Daerah tentang Pembentukan BPBD",
		},

		{
			indicator_name: "Indicator 3",
			description: "Peraturan tentang pembentukan Forum PRB",
		},

		{
			indicator_name: "Indicator 4",
			description: "Peraturan tentang penyebaran informasi kebencanaan",
		},

		{
			indicator_name: "Indicator 5",
			description: "Peraturan Daerah tentang RPB",
		},

		{
			indicator_name: "Indicator 6",
			description: "Peraturan Daerah tentang Tataruang Berbasis PRB",
		},

		{
			indicator_name: "Indicator 7",
			description: "BPBD",
		},

		{
			indicator_name: "Indicator 8",
			description: "Forum PRB",
		},
		
		{
			indicator_name: "Indicator 9",
			description: "Komitmen DPRD terhadap PRB",
		},

		{
			indicator_name: "Indicator 10",
			description: "Peta Bahaya dan kajiannya untuk seluruh bahaya yang ada di daerah",
		},
		{
			indicator_name: "Indicator 11",
			description: "Peta Kerentanan dan kajiannya untuk seluruh bahaya yang ada di daerah",
		},

		{
			indicator_name: "Indicator 12",
			description: "Peta Kapasitas dan kajiannya",
		},

		{
			indicator_name: "Indicator 13",
			description: "Rencana Penanggulangan Bencana Daerah",
		},

		{
			indicator_name: "Indicator 14",
			description: "Sarana penyampaian informasi kebencanaan yang menjangkau langsung masyarakat",
		},

		{
			indicator_name: "Indicator 15",
			description: "Sosialisasi pencegahan dan kesiapsiagaan bencana pada tiap-tiap kecamatan di wilayahnya",
		},

		{
			indicator_name: "Indicator 16",
			description: "Komunikasi bencana lintas lembaga minimal beranggotakan lembaga-lembaga dari sektor pemerintah, masyarakat mau pun dunia usaha",
		},

		{
			indicator_name: "Indicator 17",
			description: "Pusdalops PB dengan fasilitas minimal mampu memberikan respon efektif untuk pelaksanaan peringatan dini dan penanganan masa krisis",
		},

		{
			indicator_name: "Indicator 18",
			description: "Sistem pendataan bencana yang terhubung dengan sistem pendataan bencana nasional",
		},

		{
			indicator_name: "Indicator 19",
			description: "Pelatihan dan sertifikasi penggunaan peralatan PB",
		},

		{
			indicator_name: "Indicator 20",
			description: "Penyelenggaraan Latihan (geladi) Kesiapsiagaan",
		},
		{
			indicator_name: "Indicator 21",
			description: "Kajian kebutuhan peralatan dan logistik kebencanaan",
		},

		{
			indicator_name: "Indicator 22",
			description: "Pengadaan kebutuhan peralatan dan logistik kebencanaan",
		},

		{
			indicator_name: "Indicator 23",
			description: "Penyimpanan/pergudang Logistik PB",
		},

		{
			indicator_name: "Indicator 24",
			description: "Pemeliharaan peralatan dan supply chain logistik yang diselenggarakan secara periodik",
		},

		{
			indicator_name: "Indicator 25",
			description: "Tersedianya energi listrik untuk kebutuhan darurat",
		},

		{
			indicator_name: "Indicator 26",
			description: "Kemampuan pemenuhan pangan daerah untuk kebutuhan darurat",
		},

		{
			indicator_name: "Indicator 27",
			description: "Penataan ruang berbasis PRB",
		},

		{
			indicator_name: "Indicator 28",
			description: "Informasi penataan ruang yang mudah diakses publik",
		},

		{
			indicator_name: "Indicator 29",
			description: "Sekolah/Madrasah Aman Bencana (SMAB)",
		},

		{
			indicator_name: "Indicator 30",
			description: "RSAB dan Puskesmas Aman Bencana",
		},
		{
			indicator_name: "Indicator 31",
			description: "Desa Tangguh Bencana",
		},

		{
			indicator_name: "Indicator 32",
			description: "Penerapan sumur resapan dan/atau biopori",
		},

		{
			indicator_name: "Indicator 33",
			description: "Perlindungan daerah tangkapan air",
		},

		{
			indicator_name: "Indicator 34",
			description: "Restorasi sungai",
		},

		{
			indicator_name: "Indicator 35",
			description: "Penguatan lereng",
		},

		{
			indicator_name: "Indicator 36",
			description: "Penegakan hukum",
		},

		{
			indicator_name: "Indicator 37",
			description: "Optimalisasi pemanfaatan air permukaan",
		},

		{
			indicator_name: "Indicator 38",
			description: "Pemantauan berkala hulu sungai",
		},

		{
			indicator_name: "Indicator 39",
			description: "Penerapan Bangunan Tahan Gempabumi",
		},

		{
			indicator_name: "Indicator 40",
			description: "Tanaman dan/atau bangunan penahan gelombang tsunami",
		},
		{
			indicator_name: "Indicator 41",
			description: "Revitalisasi tanggul, embung, waduk dan taman kota",
		},

		{
			indicator_name: "Indicator 42",
			description: "Restorasi  lahan gambut",
		},

		{
			indicator_name: "Indicator 43",
			description: "Konservasi vegetatif DAS rawan longsor",
		},

		{
			indicator_name: "Indicator 44",
			description: "Rencana Kontijensi Gempabumi",
		},

		{
			indicator_name: "Indicator 45",
			description: "Rencana Kontijensi Tsunami",
		},

		{
			indicator_name: "Indicator 46",
			description: "Sistem Peringatan Dini Bencana Tsunami",
		},

		{
			indicator_name: "Indicator 47",
			description: "Rencana Evakuasi Bencana Tsunami",
		},

		{
			indicator_name: "Indicator 48",
			description: "Rencana kontijensi banjir",
		},

		{
			indicator_name: "Indicator 49",
			description: "Sistem peringatan dini bencana banjir",
		},

		{
			indicator_name: "Indicator 50",
			description: "Rencana kontijensi tanah longsor",
		},
		{
			indicator_name: "Indicator 51",
			description: "Sistem peringatan dini bencana tanah longsor",
		},

		{
			indicator_name: "Indicator 52",
			description: "Rencana Kontijensi karkahut",
		},

		{
			indicator_name: "Indicator 53",
			description: "Sistem peringatan dini bencana karlahut",
		},

		{
			indicator_name: "Indicator 54",
			description: "Rencana kontijensi erupsi gunungapi",
		},

		{
			indicator_name: "Indicator 55",
			description: "Sistem peringatan dini bencana erupsi gunungapi",
		},

		{
			indicator_name: "Indicator 56",
			description: "Infrastruktur evakuasi bencana erupsi gunungapi",
		},

		{
			indicator_name: "Indicator 57",
			description: "Rencana kontijensi kekeringan",
		},

		{
			indicator_name: "Indicator 58",
			description: "Sistem peringatan dini bencana kekeringan",
		},

		{
			indicator_name: "Indicator 59",
			description: "Rencana kontijensi banjir bandang",
		},

		{
			indicator_name: "Indicator 60",
			description: "Sistem peringatan dini bencana banjir bandang",
		},
		{
			indicator_name: "Indicator 61",
			description: "Penentuan Status Tanggap Darurat",
		},

		{
			indicator_name: "Indicator 62",
			description: "Penerapan sistem komando operasi darurat",
		},

		{
			indicator_name: "Indicator 63",
			description: "Pengerahan Tim Kaji Cepat ke lokasi bencana",
		},

		{
			indicator_name: "Indicator 64",
			description: "Pengerahan Tim Penyelamatan dan Pertolongan Korban",
		},

		{
			indicator_name: "Indicator 65",
			description: "Perbaikan Darurat",
		},

		{
			indicator_name: "Indicator 66",
			description: "Pengerahan bantuan pada masyarakat terjauh",
		},

		{
			indicator_name: "Indicator 67",
			description: "Penghentian status Tanggap Darurat",
		},

		{
			indicator_name: "Indicator 68",
			description: "Pemulihan pelayanan dasar pemerintah",
		},

		{
			indicator_name: "Indicator 69",
			description: "Pemulihan infrastruktur penting",
		},

		{
			indicator_name: "Indicator 70",
			description: "Perbaikan rumah penduduk",
		},

		{
			indicator_name: "Indicator 71",
			description: "Pemulihan Penghidupan masyarakat",
		},
	],

	questionnaire: [
		{
			number_of_questionnaire: 1,
			fill_in_questionnaire: "Apakah kabupaten/kota sudah ada inisiatif penyusunan Perda melibatkan pemangku kebijakan di kabupaten/kota?",
		},

		{
			number_of_questionnaire: 2,
			fill_in_questionnaire: "Apakah perda PB tersebut telah didukung oleh aturan turunan yang menjabarkan penyelenggaraan PB di daerah?",
		},

		{
			number_of_questionnaire: 3,
			fill_in_questionnaire: "Apakah Perda PB tersebut telah menjadi acuan dalam regulasi dan kebijakan lainnya dalam penyelenggaraan penanggulangan bencana?",
		},

		{
			number_of_questionnaire: 4,
			fill_in_questionnaire: "Apakah Perda PB tersebut telah diadopsi dalam kebijakan daerah lainnya dan selaras dan/atau diadopsi dalam kebijakan (seperti Perda RTRW, IMB, perijinan kawasan industry, dll)?",
		},

		{
			number_of_questionnaire: 5,
			fill_in_questionnaire: "Apakah telah ada inisiasi untuk menyusun Perda SOTK?",
		},

		{
			number_of_questionnaire: 6,
			fill_in_questionnaire: "Apakah BPBD di daerah anda telah diperkuat dalam sebuah aturan/regulasi?",
		},

		{
			number_of_questionnaire: 7,
			fill_in_questionnaire: "Apakah aturan pembentukan BPBD meningkatkan fungsi koordinasi, komando, dan pelaksanaan dalam penyelenggaraan PB di daerah?",
		},

		{
			number_of_questionnaire: 8,
			fill_in_questionnaire: "Apakah aturan pembentukan BPBD meningkatkan upaya penyelenggaraan PB di daerah?",
		},

		{
			number_of_questionnaire: 9,
			fill_in_questionnaire: "Apakah telah ada inisiatif untuk membentuk FPRB melibatkan seluruh lapisan masyarakat?",
		},

		{
			number_of_questionnaire: 10,
			fill_in_questionnaire: "Apakah telah ada diskusi-diskusi antar kelompok (baik pemerintah, LSM, PMI, Akademisi, Media, Ulama dan sebagainya)untuk menyusun aturan dan mekanisme pembentukan Forum Pengurangan Risiko bencana daerah",
		},

		{
			number_of_questionnaire: 11,
			fill_in_questionnaire: "Apakah aturan dan mekanisme yang dibuat dan disepakati tersebut digunakan dalam membentuk FPRB?",
		},

		{
			number_of_questionnaire: 12,
			fill_in_questionnaire: "Apakah aturan dan mekanisme tersebut telah berfungsi untuk mempercepat upaya PRB di daerah anda?",
		},

		{
			number_of_questionnaire: 13,
			fill_in_questionnaire: "Apakah daerah anda telah mempunyai mekanisme atau prosedur penyebaran Informasi Kebencanaan?",
		},

		{
			number_of_questionnaire: 14,
			fill_in_questionnaire: "Apakah mekanisme atau prosedur tersebut telah diperkuat dengan aturan daearah tentang penyebaran informasi kebencanaan?",
		},

		{
			number_of_questionnaire: 15,
			fill_in_questionnaire: "Apakah mekanisme dan prosedur penyebaran Informasi Kebencanaan yang di daerah anda telah terintegrasi dengan system informasi kebencanaan di tingkat nasional?",
		},

		{
			number_of_questionnaire: 16,
			fill_in_questionnaire: "Apakah peran swasta dan masyarakat sudah terakomodir dalam mekanisme atau prosedur tentang penyebaran informasi kebencanaan?",
		},

		{
			number_of_questionnaire: 17,
			fill_in_questionnaire: "Apakah daerah anda telah mempunyai Rencana Penanggulangan Bencana?",
		},

		{
			number_of_questionnaire: 18,
			fill_in_questionnaire: "Apakah Rencana Penanggulangan Bencana tersebut telah diperkuat melalui regulasi Daerah tentang Rencana Penanggulangan Bencana?",
		},

		{
			number_of_questionnaire: 19,
			fill_in_questionnaire: "Apakah Rencana Penanggulangan Bencana telah memberikan peningkatan anggaran penanggulangan bencana di daerah anda?",
		},

		{
			number_of_questionnaire: 20,
			fill_in_questionnaire: "Apakah Rencana Penanggulangan Bencana tersebut disusun berdasarkan hasil Pengkajian Risiko Bencana dan disusun secara partisipatif melibatkan multipihak?",
		},

		{
			number_of_questionnaire: 21,
			fill_in_questionnaire: "Apakah Perda RTRW anda sudah mempertimbangkan informasi ancaman bencana?",
		},

		{
			number_of_questionnaire: 22,
			fill_in_questionnaire: "Apakah proses penyusunan RTRW (persiapan-pengumpulan data-analisis data-konsepsi spasial) telah mempertimbangkan prinsip-prinsip PRB?",
		},

		{
			number_of_questionnaire: 23,
			fill_in_questionnaire: "Apakah aturan terkait tataguna lahan dan pendirian bangunan sudah mempertimbangkan prinsip PRB?",
		},

		{
			number_of_questionnaire: 24,
			fill_in_questionnaire: "Apakah ada tindakan hukum terhadap pelanggaran peruntukan tataruang?",
		},

		{
			number_of_questionnaire: 25,
			fill_in_questionnaire: "Apakah telah terbentuk BPBD di daerah Anda?",
		},

		{
			number_of_questionnaire: 26,
			fill_in_questionnaire: "Apakah kelengkapan struktur di BPBD sudah terpenuhi sesuai dengan Permendagri nomor 46 tahun 2010?",
		},

		{
			number_of_questionnaire: 27,
			fill_in_questionnaire: "Apakah kebutuhan sumber daya BPBD (dana, sarana, prasarana, personil) telah terpenuhi baik dalam hal kualitas atau kuantitasnya?",
		},

		{
			number_of_questionnaire: 28,
			fill_in_questionnaire: "Apakah BPBD telah berfungsi secara efektif dalam mengoordinasikan, memberi komando, para SKPD terkait dalam penyelenggaraan PB.",
		},

		{
			number_of_questionnaire: 29,
			fill_in_questionnaire: "Apakah telah ada forum yang terdiri dari berbagai komponen/ kelompok (baik pemerintah daerah, LSM, PMI, Akademisi, Media, kelompok agama dan sebagainya)untuk pengurangan risiko bencana daerah?",
		},

		{
			number_of_questionnaire: 30,
			fill_in_questionnaire: "Apakah forum pengurangan risiko bencana (FPRB) telah memiliki dokumen legal sebagai dasar untuk mendapatkan pengakuan secara formal dalam upaya PRB?",
		},

		{
			number_of_questionnaire: 31,
			fill_in_questionnaire: "Apakah FPRB telah memiliki mekanisme organisasi sebagai dasar dalam pencapaian tujuan dan menjalankan fungsi FPRB?",
		},

		{
			number_of_questionnaire: 32,
			fill_in_questionnaire: "Apakah FPRB di daerah anda telah menjalankan fungsi dalam mencapai tujuan forum melalui program kerja yang didukung oleh pendanaan yang jelas?",
		},

		{
			number_of_questionnaire: 33,
			fill_in_questionnaire: "Apakah ada keterlibatan kelembagaan DPRD dalam kegiatan terkait PRB atau apakah DPRD mengakomodasi usulan kegiatan terkait denan PRB?",
		},

		{
			number_of_questionnaire: 34,
			fill_in_questionnaire: "Apakah ada respon positif dari DPRD dalam pembahasan anggaran terkait PRB di daerah?",
		},

		{
			number_of_questionnaire: 35,
			fill_in_questionnaire: "Apakah DPRD menjalankan fungsi pengawasan dalam pengurangan risiko bencana?",
		},

		{
			number_of_questionnaire: 36,
			fill_in_questionnaire: "Apakah DPRD menggunakan Dana Aspirasi untuk kegiatan terkait PRB?",
		},

		{
			number_of_questionnaire: 37,
			fill_in_questionnaire: "Apakah daerah anda telah memiliki data dan informasi yang mencukupi tentang karakterisitik ancaman bencana yang ada di wilayah anda?",
		},

		{
			number_of_questionnaire: 38,
			fill_in_questionnaire: "Apakah data dan informasi tentang karakteristik ancaman bencana telah tersedia dalam bentuk peta bahaya dan kajiannya yang mampu menggambarkan jumlah potensi luas bahaya?",
		},

		{
			number_of_questionnaire: 39,
			fill_in_questionnaire: "Apakah peta bahaya yang dimiliki telah digunakan untuk menyusun kajian risiko bencana yang menghasilkan rekomendasi kebijakan penanggulangan bencana?",
		},

		{
			number_of_questionnaire: 40,
			fill_in_questionnaire: "Apakah Kajian ancaman bencana jenis hidrometeorologis (banjir, tanah longsor, kebakaran hutan dan kekeringan) telah mempertimbangkan komponen, perubahan-perubahan variabelitas iklim dan scenario iklim dan menjadi dasar penyusunan Dokumen Kajian Risikodi daerah Anda?",
		},

		{
			number_of_questionnaire: 41,
			fill_in_questionnaire: "Apakah daerah anda telah tersedia data dan informasi yang mencukupi untuk mengetahui karakterisitik kerentanan dari ancaman bencana yang ada?",
		},

		{
			number_of_questionnaire: 42,
			fill_in_questionnaire: "Apakah data dan informasi yang tersedia telah dibuat dalam bentuk peta kerentanan yang mampu menggambarkan jumlah penduduk terpapar dan potensi kerugian dari setiap jenis ancaman bencana yang ada di daerah anda?",
		},

		{
			number_of_questionnaire: 43,
			fill_in_questionnaire: "Apakah dokumen kajian dan peta kerentanan dari setiap ancaman bencana menghasilkan rekomendasi kebijakan penanggulangan bencana di daerah anda?",
		},

		{
			number_of_questionnaire: 44,
			fill_in_questionnaire: "Apakah kajian kerentanan dan peta kerentanan yang ada telah serta rekomendasi yang dihasilkan telah menjadi dasar pertimbangan dalam penyusunan rencana penanggulangan bencana di daerah anda?",
		},

		{
			number_of_questionnaire: 45,
			fill_in_questionnaire: "Apakah telah tersedia data dan informasi yang mencukupi untuk mengetahui tingkat kapasitas dari tiap-tiap ancaman bencana yang ada di daerah anda?",
		},

		{
			number_of_questionnaire: 46,
			fill_in_questionnaire: "Apakah data dan informasi tersebut telah tersedia dalam bentuk peta kapasitas yang mampu menggambarkan kemampuan daerah anda terhadap jenis-jenis ancaman bencana yang ada?",
		},

		{
			number_of_questionnaire: 47,
			fill_in_questionnaire: "Apakah dokumen dan peta kapasitas telah dianalisis dan menghasilkan rekomendasi kebijakan penanggulangan bencana?",
		},

		{
			number_of_questionnaire: 48,
			fill_in_questionnaire: "Apakah kajian risiko bencana (dokumen kajian dan peta risiko bencana) telah mempertingkan analisis dampak perubahan iklim dan menjadi dasar/acuan dalam penyusunan rencana penanggulangan bencana?",
		},

		{
			number_of_questionnaire: 49,
			fill_in_questionnaire: "Apakah dareah anda telah memiliki Dokumen Rencana Penanggulangan Bencana yang disusun berdasarkan hasil Pengkajian Risiko Bencana di daerah anda?",
		},

		{
			number_of_questionnaire: 50,
			fill_in_questionnaire: "Apakah proses penyusunan Dokumen Rencana Penanggulangan Bencana telah melibatkan dan mengakomodir lintas SKPD, aspirasi masyarakat, akademisi, dunia usaha, maupun organisasi non pemerintah dalam upaya penanggulangan bencana di daerah?",
		},

		{
			number_of_questionnaire: 51,
			fill_in_questionnaire: "Apakah Dokumen Rencana Penanggulangan Bencana tersebut telah ditetapkan dalam suatu aturan daerah untuk implementasinya?",
		},

		{
			number_of_questionnaire: 52,
			fill_in_questionnaire: "Apakah Dokumen Rencana Penanggulangan Bencana telah menjadi acuan bagi Satuan Kerja Pemerintah Daerah (SKPD) terkait dalam penyusunan perencanaan serta mendapatkan dukungan Legislatif?",
		},

		{
			number_of_questionnaire: 53,
			fill_in_questionnaire: "Apakah sudah tersedia pengaturan tentang penyebaran data dan informasi tentang kejadian kebencanaan di daerah yang disampaikan ke masyarakat?",
		},

		{
			number_of_questionnaire: 54,
			fill_in_questionnaire: "Apakah data – data yang ada diolah sebagai informasi dan informasi bencana yang diperbarui secara periodik dari sumber informasi tersebut?",
		},
		{
			number_of_questionnaire: 55,
			fill_in_questionnaire: "Apakah informasi kebencanaan yang sudah diolah telah menjadi dasar untuk pengambilan keputusan dan disampaikan kepada multi stakeholder?",
		},

		{
			number_of_questionnaire: 56,
			fill_in_questionnaire: "Apakah informasi kejadian bencana tersebut sudah dapat terintegrasi antar sector dan sudah dimanfaatkan masyarakat sebagai acuan dalam membentuk scenario operasi kebencanaan yang berpotensi terjadi?",
		},

		{
			number_of_questionnaire: 57,
			fill_in_questionnaire: "Apakah ada kegiatan sosialisasi pencegahan dan kesiapsiagaan bencana pada tiap-tiap kecamatan di wilayah anda?",
		},

		{
			number_of_questionnaire: 58,
			fill_in_questionnaire: "Apakah kegiatan sosialisasi pencegahan dan kesiapsiagaan bencana dilakukan secara rutin dan telah menjangkau seluruh lapisan masyarakat pada setiap kecamatan yang ada dengan isi materi yang terstandarkan yang disesuaikan dengan ancaman di daerah Anda?",
		},

		{
			number_of_questionnaire: 59,
			fill_in_questionnaire: "Apakah masyarakat sudah berperilaku dan berbudaya untuk melakukan sosialisasi pencegahan dan kesiapsiagaan secara mandiri?",
		},

		{
			number_of_questionnaire: 60,
			fill_in_questionnaire: "Apakah dengan adanya sosialisasi tersebut masyarakat sudah mampu mengimplementasikan upaya pencegahan dan kesiapsiagaan yang dilakukan secara mandiri oleh masyarakat, misalnya rencana evakuasi, EWS.",
		},

		{
			number_of_questionnaire: 61,
			fill_in_questionnaire: "Apakah ada mekanisme bersama yang menjalankan peran bagi-guna data dan informasi kebencanaan?",
		},

		{
			number_of_questionnaire: 62,
			fill_in_questionnaire: "Apakah mekanisme tersebut didukung dengan aturan dan sumberdaya yang memadai?",
		},

		{
			number_of_questionnaire: 63,
			fill_in_questionnaire: "Apakah hasil dari mekanisme bersama tersebut sudah saling memanfaatkan pada masing – masing stakeholder?",
		},

		{
			number_of_questionnaire: 64,
			fill_in_questionnaire: "Apakah mekanisme bersama tersebut sudah dapat menghasilkan program bersama secara terstruktur dan berkelanjutan.",
		},

		{
			number_of_questionnaire: 65,
			fill_in_questionnaire: "Apakah telah ada Pusat Pengendali Operasi (Pusdalops) atau Sistem Komando Tanggap Darurat (SKTD) Bencana yang terstruktur dalam sebuah prosedur operasi di daerah anda?",
		},

		{
			number_of_questionnaire: 66,
			fill_in_questionnaire: "Apakah pusdalops sudah didukung peralatan yang memadai (sesuai dengan minimal standar perka BNPB) untuk menjalankan fungsi peringatan dini dan penanganan masa krisis?",
		},

		{
			number_of_questionnaire: 67,
			fill_in_questionnaire: "Apakah pusdalops sudah didukung peralatan yang memadai (sesuai dengan minimal standar perka BNPB) untuk menjalankan fungsi peringatan dini dan penanganan masa krisis? Catatan: diukur dengan peralatan, satu komando, menjalankan sesuai SKTD",
		},

		{
			number_of_questionnaire: 68,
			fill_in_questionnaire: "Apakah efektitivitas yang dimiliki Pusdalops ataupun SKPDB di atas dapat dijadikan acuan untuk perencanaan tanggap darurat selanjutnya?",
		},

		{
			number_of_questionnaire: 69,
			fill_in_questionnaire: "Apakah telah ada sarana dan prasarana yang mendukung sistem pendataan bencana yang terhubung dengan sistem pendataan bencana nasional?",
		},

		{
			number_of_questionnaire: 70,
			fill_in_questionnaire: "Apakah system pendataan di tingkat nasional dan di tingkat daerah dapat saling memanfaatkan?",
		},

		{
			number_of_questionnaire: 71,
			fill_in_questionnaire: "Apakah system pendataan nasional yang terintegrasi dengan system di daerah ikut membangun rencana scenario pencegahan dan kesiapsiagaan di daerah?",
		},

		{
			number_of_questionnaire: 72,
			fill_in_questionnaire: "Apakah system pendataan nasional yang terntegrasi dengan system di daerah tersebut dimanfaakan di daerah untuk mendukung perencanaan, pembuatan keputusan, serta program/kegiatan di daerah Anda?",
		},

		{
			number_of_questionnaire: 73,
			fill_in_questionnaire: "Apakah telah dilakukan peningkatan kapasitas, pelatihan, sertifikasi penggunaan peralatan PB secara rutin/ berkala (minimal 2 kali dalam setahun) di daerah anda?",
		},

		{
			number_of_questionnaire: 74,
			fill_in_questionnaire: "Apakah hasil pelatihan dan sertifikasi penggunaan peralatan PB telah diuji coba dalam sebuah latihan kesiapsiagaan (drill, simulasi, geladi posko, maupun geladi lapang)",
		},

		{
			number_of_questionnaire: 75,
			fill_in_questionnaire: "Apakah dengan sertifikasi penggunaaan peralatan PB tersebut, personil dapat merespon kejadian bencana di daerah sesuai dengan SKPDB?",
		},

		{
			number_of_questionnaire: 76,
			fill_in_questionnaire: "Apakah sumberdaya yang telah tersertifikasi dipercaya sebagai pemangku kepentingan kunci dalam respons kejadian bencana.",
		},

		{
			number_of_questionnaire: 77,
			fill_in_questionnaire: "Apakah telah ada penyelenggaraan pelatihan kesiapsiagaan di daerah anda?",
		},

		{
			number_of_questionnaire: 78,
			fill_in_questionnaire: "Apakah penyelenggaraan latihan (geladi) kesiapsiagaan tersebut telah dilakukan secara bertahap dan berlanjut (mulai dari Pelatihan, Simulasi, hingga Uji Sistem)?",
		},

		{
			number_of_questionnaire: 79,
			fill_in_questionnaire: "Apakah masyarakat dan pemangku kepentingan sadar petingnya dan merasa aman dengan adanya penyelenggaraan latihan (geladi) kesiapsiagaan tersebut? Catatan: daerah perlu melakukan pengukuan/survey pra dan pasca gladi untuk mengukur rasa aman.",
		},

		{
			number_of_questionnaire: 80,
			fill_in_questionnaire: "Apakah Latihan (geladi) kesiapsiagaan tersebut telah dapat meningkatkan kapasitas masyarakat terhadap kesiapsiagaan?",
		},

		{
			number_of_questionnaire: 81,
			fill_in_questionnaire: "Apakah telah dilakukan kajian kebutuhan peralatan dan logistik kebencanaan di daerah anda?",
		},

		{
			number_of_questionnaire: 82,
			fill_in_questionnaire: "Apakah kajian kebutuhan peralatan dan logistik tersebut dilakukan berdasarkan Rencana Kontingensi atau dokumen kajian lainnya (risiko, tanggap darurat, rehabilitasi dan rekonstruksi) untuk bencana prioritas di daerah anda?",
		},

		{
			number_of_questionnaire: 83,
			fill_in_questionnaire: "Apakah hasil kajian kebutuhan peralatan dan logistik tersebut telah diintegrasikan dalam Dokumen Perencanaan Daerah di daerah anda?",
		},

		{
			number_of_questionnaire: 84,
			fill_in_questionnaire: "Apakah hasil kajian kebutuhan peralatan dan logistik yang terintegrasi dalam Dokumen Perencahaan Daerah memiliki dampak terhadap peningkatan alokasi anggaran dalam pemenuhan kebutuhan peralatan dan logistik kebencanaan di daerah anda?",
		},

		{
			number_of_questionnaire: 85,
			fill_in_questionnaire: "Apakah terdapat lembaga di pemerintahan yang menangani (mengusulkan dan atau melaksanakan) peralatan dan logistik kebencanaan untuk darurat bencana?",
		},

		{
			number_of_questionnaire: 86,
			fill_in_questionnaire: "Apakah pengadaan kebutuhan peralatan dan logistik kebencanaan dilakukan berdasarkan hasil Kajian Kebutuhan Peralatan dan Logistik Kebencanaan, sebagaimana dijelaskan pada indikator 21 (pertanyaan 81-84)",
		},

		{
			number_of_questionnaire: 87,
			fill_in_questionnaire: "Apakah pengadaan kebutuhan peralatan dan logistik kebencanaan yang dipenuhi di daerah anda telah sesuai dengan kebutuhan hasil kajian?",
		},

		{
			number_of_questionnaire: 88,
			fill_in_questionnaire: "Apakah peralatan dan logistic kebencanaan yang dipenuhi didaerah anda telah sesuai dengan kebutuhan hasil kajian dan relevan dengan kebutuhan riil saat kondisi bencana?",
		},

		{
			number_of_questionnaire: 89,
			fill_in_questionnaire: "Apakah telah ada tempat penyimpanan/pergudangan logistik di daerah anda?",
		},

		{
			number_of_questionnaire: 90,
			fill_in_questionnaire: "Apakah tempat penyimpanan/pergudangan logistik tersebut berada dibawah lembaga teknis tertentu di pemerintahan untuk penanganan darurat bencana?",
		},

		{
			number_of_questionnaire: 91,
			fill_in_questionnaire: "Apakah penyimpanan/pergudangan logistik PB yang ada mampu dijamin secara akuntabilitas dan transparansi pengelolaannya?",
		},

		{
			number_of_questionnaire: 92,
			fill_in_questionnaire: "Menurut anda, apakah kebutuhan tempat penyimpanan/pergudangan logistik di daerah anda telah terpenuhi baik dalam hal kualitas maupun kuantitasnya?",
		},

		{
			number_of_questionnaire: 93,
			fill_in_questionnaire: "Apakah terdapat lembaga di pemerintahan yang menangani pemeliharaan peralatan dan supply chain logistik yang diselenggarakan secara periodik?",
		},

		{
			number_of_questionnaire: 94,
			fill_in_questionnaire: "Apakah lembaga tersebut memiliki kemampuan sumber daya (anggaran, personil, peralatan, mekanisme dan prosedur) yang cukup dalam menangani pemeliharaan peralatan dan ketersediaan supply chain logistik untuk kebutuhan darurat bencana di daerah anda?",
		},

		{
			number_of_questionnaire: 95,
			fill_in_questionnaire: "Apakah pemeliharaan peralatan dan pemenuhan ketersediaan supply chainpada masa tanggap darurat bencana yang disusun berdasarkan hasil pengkajian risiko bencana dan/ atau hasil rencana evakuasi berjalan efektif?",
		},

		{
			number_of_questionnaire: 96,
			fill_in_questionnaire: "Menurut anda, apakah pemeliharaan peralatan dan supply chain logistik yang diselenggarakan secara periodik di daerah anda telah terpenuhi baik dalam hal kualitas maupun kuantitasnya?",
		},

		{
			number_of_questionnaire: 97,
			fill_in_questionnaire: "Apakah terdapat lembaga di pemerintahan yang bertanggungjawab menyediakan energi listrik untuk kebutuhandarurat bencana?",
		},

		{
			number_of_questionnaire: 98,
			fill_in_questionnaire: "Apakah lembaga tersebut telah memiliki mekanisme dan prosedur dalam menangani pemenuhan ketersediaan energi listrik untuk kebutuhan darurat bencana di daerah anda?",
		},

		{
			number_of_questionnaire: 99,
			fill_in_questionnaire: "Apakah strategi/mekanisme pemenuhan kebutuhan energy listrik pada masa tanggap darurat telah mempertimbangkan scenario bencana terparah yang disusun berdasarkan Rencana Kontijensi?",
		},

		{
			number_of_questionnaire: 100,
			fill_in_questionnaire: "Adakah jaminan keberlangsungan dan/atau pemulihan pasokan listrik untuk kebutuhan darurat bencana terparah di daerah?",
		},

		{
			number_of_questionnaire: 101,
			fill_in_questionnaire: "Apakah terdapat lembaga di pemerintahan yang bertanggungjawabdalam pemenuhan pangan daerah untuk kebutuhan darurat bencana?",
		},

		{
			number_of_questionnaire: 102,
			fill_in_questionnaire: "Apakah terdapat strategi pemenuhan kebutuhan pangan daerah telah mempertimbangkan scenario bencana terparah (berdasarkan Rencana Kontijensi) dan scenario bencana jangka panjang (slow onset) di daerah?",
		},

		{
			number_of_questionnaire: 103,
			fill_in_questionnaire: "Apakah strategi pemenuhan kebutuhan pangan daerah untuk kebutuhan darurat telah menjadi strategi bersama seluruh pemangku kepentingan (pemerintah-masyarakat-sektor swasta)?",
		},

		{
			number_of_questionnaire: 104,
			fill_in_questionnaire: "Adakah jaminan ketahanan pangan untuk kebutuhan darurat bencana terparah maupun risiko bencana jangka panjang (slow onset) di daerah?",
		},

		{
			number_of_questionnaire: 105,
			fill_in_questionnaire: "Apakah pemerintah kota/kab telah melakukan inisiatif penyusunan tata ruang kab/kota dalam rangka mengintegrasikan penanggulangan bencana/manajemen risiko bencana?",
		},

		{
			number_of_questionnaire: 106,
			fill_in_questionnaire: "Apakah pemerintah kota/kab telah melakukan inisiatif pengkajian kembali tata ruang kab/kota dalam rangka penanggulangan bencana/ manajemen risiko bencana secara inklusif?",
		},

		{
			number_of_questionnaire: 107,
			fill_in_questionnaire: "Apakah telah ada RTRW Kota/ Kabupaten Revisi yang mengintegrasikan dan mengakomodir kebutuhan penanggulangan bencana/ manajemenen risiko bencana?",
		},

		{
			number_of_questionnaire: 108,
			fill_in_questionnaire: "Apakah struktur ruang (pemukiman dan jaringan prasarana) dan pola ruang (kawasan lindung dan kawasan budidaya) dalam Rencana Detail Tata Ruang (RDTR) telah dimanfaatkan untuk mencegah dan/atau mengurangi keterpaparan bahaya bencana dan mendukung peningkatan kapasitas kota/kab. dalam penanggulangan bencana/ manajemen risiko bencana?",
		},

		{
			number_of_questionnaire: 109,
			fill_in_questionnaire: "Apakah telah ada lembaga pemerintah yang menangani informasi penataan ruang di daerah anda?",
		},

		{
			number_of_questionnaire: 110,
			fill_in_questionnaire: "Apakah telah ada informasi penataan ruang yang mudah diakses publik?",
		},

		{
			number_of_questionnaire: 111,
			fill_in_questionnaire: "Apakah publik telah memanfaatkan informasi penataan ruang untuk pengurangan risiko bencana?",
		},

		{
			number_of_questionnaire: 112,
			fill_in_questionnaire: "Apakah publik telah menerapkan penataan ruang untuk pengurangan risiko bencana?",
		},

		{
			number_of_questionnaire: 113,
			fill_in_questionnaire: "Apakah dilaksanakan sosialisasi kepada seluruh sekolah/ madrasah ditingkat pendidkan dasar (SD) hingga menegah (SMP) di kawasan rawan bencana - tentang hasil/ manfaat/ tujuan dari kegiatan/program sekolah dan madrasah aman bencana (SMAB)?",
		},

		{
			number_of_questionnaire: 114,
			fill_in_questionnaire: "Apakah 75% dari total jumlah sekolah/madrasah pendidikan dasar (SD) hingga menegah (SMP) di daerah rawan bencana sudah pernah melaksanakan kegiatan/program sekolah dan madrasah aman bencana?",
		},

		{
			number_of_questionnaire: 115,
			fill_in_questionnaire: "Apakah pelaksanakan kegiatan/program sekolah dan madrasah aman pendidikan dasar (SD) hingga menegah (SMP) difokuskan pada salah satu dari 3 pilar (pendidikan untuk pengurangan risiko bencana, manajemen bencana sekolah, sarana prasarana) sekolah/madrasah aman bencana? (50% dari 75% dari sekolah/madrasah yang pernah disosialisasikan)",
		},

		{
			number_of_questionnaire: 116,
			fill_in_questionnaire: "Apakah pelaksanakan kegiatan/program sekolah dan madrasah aman yang fokus pada 3 pilar (pendidikan untuk pengurangan risiko bencana, manajemen bencana sekolah, sarana prasarana) di seluruh sekolah/madrasah aman bencana yang ada di kawasan rawan bencana sudah dilakukan secara komprehensif?",
		},

		{
			number_of_questionnaire: 117,
			fill_in_questionnaire: "Apakah sosialisasi rumah sakit dan puskesmas aman bencana sudah dilakukan di daerah rawan bencana?",
		},

		{
			number_of_questionnaire: 118,
			fill_in_questionnaire: "Apakah seluruh rumah sakit daerah rawan bencana perencanaan kegiatan/program rumah sakit aman bencana sudah berdasarkan pada 4 modul safety hospital (kajian keterpaparan acaman, gedung/bangunan aman, sarana prasarana rumah sakit aman, kemampuan penyelenggaraan penanggulangan bencana.)",
		},

		{
			number_of_questionnaire: 119,
			fill_in_questionnaire: "Apakah seluruh rumah sakit daerah rawan bencana sudah melaksanakan kegiatan/program rumah sakit aman bencana berdasarkan pada 4 modul safety hospital (kajian keterpaparan acaman, gedung/bangunan aman, sarana prasarana rumah sakit aman, kemampuan penyelenggaraan penanggulangan bencana.)",
		},

		{
			number_of_questionnaire: 120,
			fill_in_questionnaire: "Apakah seluruh rumah sakit di kawasan rawan bencana telah melakukan sertifikasi / evaluasi aspek safety hospital yang berkaitan dengan pemenuhan syarat akreditasi rumah sakit?",
		},

		{
			number_of_questionnaire: 121,
			fill_in_questionnaire: "Apakah telah ada sosialisasi pengurangan risiko bencana di kawasan rawan bencana yang dilakukan kepada komunitas-komunitas masyarakat di daerah anda?",
		},

		{
			number_of_questionnaire: 122,
			fill_in_questionnaire: "Apakah telah ada dilakukan peningkatan kapasitas kelurahan/desa (destana) di desa – desa di seluruh kawasan rawan bencana?",
		},

		{
			number_of_questionnaire: 123,
			fill_in_questionnaire: "Apakah telah desa tangguh bencana tersebut telah melakukan simulasi dan apakah penerapan indicator destanan tersebut berkontribusi pada pembangunan desa berwawasan PRB?",
		},

		{
			number_of_questionnaire: 124,
			fill_in_questionnaire: "Apakah Desa Tangguh bencana tersebut telah mempu menginspirasi dan membantu pembangunan Desa Tangguh bencana di tempat lain?",
		},

		{
			number_of_questionnaire: 125,
			fill_in_questionnaire: "Apakah di pemerintahan maupun dikomunitas Anda telah ada kebijakan tentang pengelolaan lingkungan hidup (resapan air)?",
		},

		{
			number_of_questionnaire: 126,
			fill_in_questionnaire: "Apakah telah ada penerapan resapan air dalam upaya pengurangan risiko bencana banjir?",
		},

		{
			number_of_questionnaire: 127,
			fill_in_questionnaire: "Apakah penerapan resapan air di daerah anda telah menurunkan frekuensi dan luasan banjir dalam setahun terakhir?",
		},

		{
			number_of_questionnaire: 128,
			fill_in_questionnaire: "Apakah penerapan resapan air di daerah anda mampu mengurangi dampak ekonomi yang ditimbulkan oleh bencana banjir?",
		},

		{
			number_of_questionnaire: 129,
			fill_in_questionnaire: "Apakah di pemerintahan maupun dikomunitas Anda telah ada kebijakan tentang pengelolaan lingkungan hidup (daerah tangkapan air)?",
		},

		{
			number_of_questionnaire: 130,
			fill_in_questionnaire: "Apakah telah ada perlindungan daerah tangkapan air dalam upaya pengurangan risiko bencana banjir?",
		},

		{
			number_of_questionnaire: 131,
			fill_in_questionnaire: "Apakah perlindungan daerah tangkapan air di daerah anda telah menurunkan frekuensi dan luasan banjir dalam setahun terakhir?",
		},

		{
			number_of_questionnaire: 132,
			fill_in_questionnaire: "Apakah perlindungan daerah tangkapan air di daerah anda mampu mengurangi dampak ekonomi yang ditimbulkan oleh bencana banjir?",
		},

		{
			number_of_questionnaire: 133,
			fill_in_questionnaire: "Apakah di pemerintahan maupun dikomunitas Anda telah ada kebijakan tentang pengelolaan lingkungan hidup (restorasi sungai)?",
		},

		{
			number_of_questionnaire: 134,
			fill_in_questionnaire: "Apakah telah ada upaya restorasi sungai dalam upaya pengurangan risiko bencana banjir?",
		},

		{
			number_of_questionnaire: 135,
			fill_in_questionnaire: "Apakah upaya restorasi sungai di daerah anda telah menurunkan frekuensi dan luasan banjir dalam setahun terakhir?",
		},

		{
			number_of_questionnaire: 136,
			fill_in_questionnaire: "Apakah upaya restorasi sungai di daerah anda mampu mengurangi dampak ekonomi yang ditimbulkan oleh bencana banjir?",
		},

		{
			number_of_questionnaire: 137,
			fill_in_questionnaire: "Apakah di pemerintahan maupun dikomunitas Anda telah ada kebijakan tentang pengelolaan lingkungan hidup (Kawasan DAS Rawan Longsor)?",
		},

		{
			number_of_questionnaire: 138,
			fill_in_questionnaire: "Apakah telah ada upaya penguatan lereng dalam upaya pengurangan risiko bencana tanah longsor?",
		},

		{
			number_of_questionnaire: 139,
			fill_in_questionnaire: "Apakah upaya penguatan lereng di daerah anda telah menurunkan frekuensi dan luasan tanah longsor?",
		},

		{
			number_of_questionnaire: 140,
			fill_in_questionnaire: "Apakah upaya penguatan lereng di daerah anda mampu mengurangi dampak ekonomi yang ditimbulkan oleh bencana tanah longsor?",
		},

		{
			number_of_questionnaire: 141,
			fill_in_questionnaire: "Apakah telah ada Peraturan Daerah/Peraturan Adat atau desa dalam Pencegahan dan Mitigasi Bencana Kebakaran Lahan dan Hutan?",
		},

		{
			number_of_questionnaire: 142,
			fill_in_questionnaire: "Apakah telah ada penegakan hukum bagi Masyarakat, Swasta, dan Instansi yang melanggar perda tersebut?",
		},

		{
			number_of_questionnaire: 143,
			fill_in_questionnaire: "Apakah peraturan daerah sudah di implementasikan pemda dalam memfasilitasi pembukaan lahan tanpa bakar?",
		},

		{
			number_of_questionnaire: 144,
			fill_in_questionnaire: "Apakah dengan adanya peraturan dan penegakan hukum dapat mengurangi titik panas (hotspot) dan indeks kebakaran hutan dan gambut di banding dengan tahun sebelumnya?",
		},

		{
			number_of_questionnaire: 145,
			fill_in_questionnaire: "Apakah sudah ada inisiatif-inisiatif di tingkat daerah yang memadai dalam Pengelolan air permukaan (perlindungan, Pemanfaatan dan pemeliharaan) untuk pencegahan dan mitigasi dan kekeringan?",
		},

		{
			number_of_questionnaire: 146,
			fill_in_questionnaire: "Apakah sudah ada peraturan daerah yang mengatur operasionalisasi dan implementasi pengelolaan air permukaan?",
		},

		{
			number_of_questionnaire: 147,
			fill_in_questionnaire: "Apakah telah ada program optimalisasi pengelolaan air permukaan dalam upaya pencegahan dan mitigasi kekeringan?",
		},

		{
			number_of_questionnaire: 148,
			fill_in_questionnaire: "Apakah program optimalisasi program pengelolaan air telah mengurangi risiko bencana kekeringan?",
		},

		{
			number_of_questionnaire: 149,
			fill_in_questionnaire: "Apakah ada inisiatif atau keterlibatan kota/kab. dalam pengembangkan sistem pengelolan dan pemantauan area hulu DAS (pendekatan landskap, lintas administratif kota/kab.)?",
		},

		{
			number_of_questionnaire: 150,
			fill_in_questionnaire: "Apakah ada kebijakan yang mendukung inisiatif atau keterlibatan kota/kab. dalam pengembangkan sistem pengelolan dan pemantauan area hulu DAS (pendekatan landskap, lintas administratif kota/kab.)?",
		},

		{
			number_of_questionnaire: 151,
			fill_in_questionnaire: "Apakah ada kebijakan kerjasama parapihak dalam pengembangkan sistem pengelolan dan pemantauan terpadu area hulu DAS berbasis pendekatan landskap?",
		},

		{
			number_of_questionnaire: 152,
			fill_in_questionnaire: "Apakah implementasinya mengurangi risiko bencana banjir bandang?",
		},

		{
			number_of_questionnaire: 153,
			fill_in_questionnaire: "Apakah telah ada kebijakan bangunan tahan gempabumi di daerah anda?",
		},

		{
			number_of_questionnaire: 154,
			fill_in_questionnaire: "Apakah kebijakan tersebut sudah diterapkan dalam perijinan mendirikan bangunan (IMB) daerah anda?",
		},

		{
			number_of_questionnaire: 155,
			fill_in_questionnaire: "Apakah telah dilakukan pemantauan dan evaluasi terhadap penerapan IMB?",
		},

		{
			number_of_questionnaire: 156,
			fill_in_questionnaire: "Apakah ada tindakan hukum terhadap pelanggaran penerapan IMB?",
		},

		{
			number_of_questionnaire: 157,
			fill_in_questionnaire: "Apakah telah ada inisiatif mitigasi struktural (tanaman dan/atau bangunan) penahan gelombang tsunami di daerah rawan tsunami?",
		},

		{
			number_of_questionnaire: 158,
			fill_in_questionnaire: "Apakah ada regulasi (kebijakan dan peraturan kota/kab) yang mendukung inisiatif tersebut?",
		},

		{
			number_of_questionnaire: 159,
			fill_in_questionnaire: "Apakah penerapan mitigasi tersebut sudah meliputi seluruh daerah berisiko tinggi terhadap tsunami?",
		},

		{
			number_of_questionnaire: 160,
			fill_in_questionnaire: "Apakah sudah dilakukan evaluasi dan peningkatan kualitas penahan gelombang tsunami (tanaman dan/atau bangunan) secara berkala?",
		},

		{
			number_of_questionnaire: 161,
			fill_in_questionnaire: "Apakah telah ada inisiatif mitigasi struktural bencana banjir (misal revitalisasi tanggul/embung/waduk dan taman kota) di daerah anda?",
		},

		{
			number_of_questionnaire: 162,
			fill_in_questionnaire: "Apakah telah ada kebijakan yang mendukungmitigasi struktural bencana banjir (misal revitalisasi tanggul/embung/waduk dan taman kota) di daerah anda?",
		},

		{
			number_of_questionnaire: 163,
			fill_in_questionnaire: "Apakah telah dilakukan upaya mitigasi struktural bencana banjir (misal revitalisasi tanggul/embung/waduk) di daerah anda?",
		},

		{
			number_of_questionnaire: 164,
			fill_in_questionnaire: "Apakah sudah dilakukan evaluasi dan peningkatan kualitas mitigasi struktural bencana banjir (misal revitalisasi tanggul/embung/waduk) secara berkala dengan mempertimbangkan dampak perubahan iklim?",
		},

		{
			number_of_questionnaire: 165,
			fill_in_questionnaire: "Apakah telah ada kebijakan tentang pengelolaan lahan gambut di daerah anda?",
		},

		{
			number_of_questionnaire: 166,
			fill_in_questionnaire: "Apakah inisiatif pengelolaan dan restorasi lahan gambut telah dilaksanakan bersama antara pemerintah dan swasta?",
		},

		{
			number_of_questionnaire: 167,
			fill_in_questionnaire: "Apakah telah ada kebijakan tentang restorasi lahan gambut di daerah anda?",
		},

		{
			number_of_questionnaire: 168,
			fill_in_questionnaire: "Apakah telah ada program dan kegiatan restorasi lahan gambut?",
		},

		{
			number_of_questionnaire: 169,
			fill_in_questionnaire: "Apakah telah ada inisiatif mitigasi struktural bencana longsor (misal konservasi vegetatif di DAS) di daerah anda?",
		},

		{
			number_of_questionnaire: 170,
			fill_in_questionnaire: "Apakah telah ada kebijakan tentang konservasi vegetatif DAS di wilayah rawan longsor daerah anda?",
		},

		{
			number_of_questionnaire: 171,
			fill_in_questionnaire: "Apakah telah ada program dan kegiatan konservasi vegetatif di wilayah DAS yang rawa longsor secara berkelanjutan?",
		},

		{
			number_of_questionnaire: 172,
			fill_in_questionnaire: "Apakah sudah dilakukan evaluasi dan peningkatan kualitas konservasi vegetatif di wilayah DAS rawan longsor secara berkala dengan mempertimbangkan dampak perubahan iklim?",
		},

		{
			number_of_questionnaire: 173,
			fill_in_questionnaire: "Apakah sudah ada inisiatif penyusunan rencana kontijensi untuk bencana gempabumi di daerah anda? (ditambahkan catatan mengenai petingnya keterlibatan multipihak dalam proses penyusunannya)",
		},

		{
			number_of_questionnaire: 174,
			fill_in_questionnaire: "Apakah Rencana kontijensi yang disusun telah disahkan dan tersinkronisasi dengan Prosedur Tetap Penangan Darurat Bencana atau Rencana Penanggulangan Kedaruratan Bencana Gempabumi?",
		},

		{
			number_of_questionnaire: 175,
			fill_in_questionnaire: "Apakah Rencana Kontijensi yang disusun mampu dijalankan pada masa krisis dan diturunkan menjadi Rencana Operasi pada masa tanggap darurat bencana gempabumi?",
		},

		{
			number_of_questionnaire: 176,
			fill_in_questionnaire: "Apakah Rencana Kontijensi Gempabumi ini telah mempengaruhi kebijakan anggaran di daerah Anda?",
		},

		{
			number_of_questionnaire: 177,
			fill_in_questionnaire: "Apakah ada sudah ada inisiatif untuk membangun sistem peringatan dini tsunami di daerah Anda? Catatan: Terdapat peran aktif dari pemerintah daerah dalam inisiasi dan pengembangan system EWS yang diberikan oleh pemerintah pusat/donor.",
		},

		{
			number_of_questionnaire: 178,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji sistem dan prosedur peringatan dini tsunami secara berkala oleh multi stakeholder di daerah anda?",
		},

		{
			number_of_questionnaire: 179,
			fill_in_questionnaire: "Apakah sistem peringatan dini yang dibangun dapat turut meningkatkan kesadaran masyarakat akan bahaya tsunami?",
		},

		{
			number_of_questionnaire: 180,
			fill_in_questionnaire: "Apakah system peringatan ini sudah dapat meningkatkan kesiapsiagaan masyarakat dan dunia usaha dari ancaman Tsunami?",
		},

		{
			number_of_questionnaire: 181,
			fill_in_questionnaire: "Apakah ada sudah ada inisiatif untuk membangun sistem peringatan dini tsunami di daerah Anda?",
		},

		{
			number_of_questionnaire: 182,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji sistem dan prosedur peringatan dini tsunami secara berkala oleh multi stakeholder di daerah anda?",
		},

		{
			number_of_questionnaire: 183,
			fill_in_questionnaire: "Apakah sistem peringatan dini yang dibangun dapat turut meningkatkan kesadaran masyarakat akan bahaya tsunami?",
		},

		{
			number_of_questionnaire: 184,
			fill_in_questionnaire: "Apakah sistem peringatan dini ini sudah dapat menimbulkan rasa aman masyarakat (dan investor) dari ancaman tsunami.",
		},

		{
			number_of_questionnaire: 185,
			fill_in_questionnaire: "Apakah sudah ada inisiatif rencana evakuasi bencana tsunami yang disusun berdasarkan hasil rencana kontijensi dan memperhitungkan aksesibilitas pengungsi?",
		},

		{
			number_of_questionnaire: 186,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji system rencana evakuasi secara berkala oleh multi stakeholder (minimal 1 tahun sekali)?",
		},

		{
			number_of_questionnaire: 187,
			fill_in_questionnaire: "Apakah masyarakat mampu memperbaharui rencana evakuasi tersebut secara mandiri dan berkala?",
		},

		{
			number_of_questionnaire: 188,
			fill_in_questionnaire: "Apakah seluruh masyarakat di daerah rawan bencana tsunami mampu menerapkan rencana evakuasi tersebut?",
		},

		{
			number_of_questionnaire: 189,
			fill_in_questionnaire: "Apakah sudah ada inisiatif penyusunan rencana kontijensi untuk bencana Banjir di daerah anda? (ditambahkan catatan mengenai petingnya keterlibatan multipihak dalam proses penyusunannya)",
		},

		{
			number_of_questionnaire: 190,
			fill_in_questionnaire: "Apakah rencana kontingensi yang disusun telah disahkan dan tersinkronisasi dengan Prosedur Tetap Peringatan Dini dan Penanganan Darurat Bencana banjir?",
		},

		{
			number_of_questionnaire: 191,
			fill_in_questionnaire: "Apakah Rencana Kontingensi yang disusun mampu dijalankan pada masa krisis dan diturunkan menjadi Rencana Operasi pada masa tanggap darurat bencana banjir?",
		},

		{
			number_of_questionnaire: 192,
			fill_in_questionnaire: "Apakah Rencana Kontijensi Banjir ini dapat mempengaruhi kebijakan anggaran di daerah Anda?",
		},

		{
			number_of_questionnaire: 193,
			fill_in_questionnaire: "Apakah ada sudah ada inisiatif untuk membangun sistem peringatan dini banjir di daerah Anda? Catatan: Terdapat peran aktif dari pemerintah daerah dalam inisiasi dan pengembangan system EWS yang diberikan oleh pemerintah pusat/donor.",
		},

		{
			number_of_questionnaire: 194,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji sistem dan prosedur peringatan dini banjir secara berkala oleh multi stakeholder di daerah anda?",
		},

		{
			number_of_questionnaire: 195,
			fill_in_questionnaire: "Apakah sistem peringatan dini yang dibangun dapat turut meningkatkan kesadaran masyarakat akan bahaya banjir?",
		},

		{
			number_of_questionnaire: 196,
			fill_in_questionnaire: "Apakah system peringatan ini sudah dapat meningkatkan kesiapsiagaan masyarakat dan dunia usaha dari ancaman Banjir?",
		},

		{
			number_of_questionnaire: 197,
			fill_in_questionnaire: "Apakah sudah ada inisiatif penyusunan rencana kontijensi untuk bencana Longsor di daerah anda?",
		},

		{
			number_of_questionnaire: 198,
			fill_in_questionnaire: "Apakah rencana kontingensi yang disusun telah disahkan dan tersinkronisasi dengan Prosedur Tetap Peringatan Dini dan Penanganan Darurat Bencana tanah longsor?",
		},

		{
			number_of_questionnaire: 199,
			fill_in_questionnaire: "Apakah Rencana Kontingensi yang disusun telah mampu dijalankan pada masa krisis dan diturunkan menjadi Rencana Operasi pada masa tanggap darurat bencana tanah longsor?",
		},

		{
			number_of_questionnaire: 200,
			fill_in_questionnaire: "Apakah Rencana Kontijensi Longsor ini telah mempengaruhi kebijakan anggaran di daerah Anda?",
		},

		{
			number_of_questionnaire: 201,
			fill_in_questionnaire: "Apakah ada sudah ada inisiatif untuk membangun sistem peringatan dini tanah longsor di daerah Anda? Catatan: Terdapat peran aktif dari pemerintah daerah dalam inisiasi dan pengembangan system EWS yang diberikan oleh pemerintah pusat/donor",
		},

		{
			number_of_questionnaire: 202,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji sistem dan prosedur peringatan dini tanah longsor secara berkala oleh multi stakeholder di daerah anda?",
		},

		{
			number_of_questionnaire: 203,
			fill_in_questionnaire: "Apakah sistem peringatan dini yang dibangun dapat turut meningkatkan kesadaran masyarakat akan bahaya tanah longsor?",
		},

		{
			number_of_questionnaire: 204,
			fill_in_questionnaire: "Apakah system peringatan ini sudah dapat meningkatkan kesiapsiagaan masyarakat dan dunia usaha dari ancaman tanah longsor?",
		},

		{
			number_of_questionnaire: 205,
			fill_in_questionnaire: "Apakah sudah ada inisiatif penyusunan rencana kontijensi untuk bencana Kebakaran Lahan dan Hutan di daerah anda?",
		},

		{
			number_of_questionnaire: 206,
			fill_in_questionnaire: "Apakah rencana kontijensi yang disusun telah disahkan dan tersinkronisasi dengan Prosedur Tetap Peringatan Dini dan Penanganan Darurat Bencana kebakaran hutan dan lahan?",
		},

		{
			number_of_questionnaire: 207,
			fill_in_questionnaire: "Apakah Rencana Kontingensi yang disusun telah diujicoba, dievaluasi, dan terbukti mampu dijalankan pada masa krisis dan diturunkan menjadi Rencana Operasi pada masa tanggap darurat bencana kebakaran hutan dan lahan?",
		},

		{
			number_of_questionnaire: 208,
			fill_in_questionnaire: "Apakah Rencana Kontijensi Kebakaran Lahan dan Hutan ini telah mempengaruhi kebijakan anggaran di daerah Anda?",
		},

		{
			number_of_questionnaire: 209,
			fill_in_questionnaire: "Apakah ada sudah ada inisiatif untuk membangun sistem peringatan dini Kebakaran lahan dan hutan di daerah Anda? Catatan: Terdapat peran aktif dari pemerintah daerah dalam inisiasi dan pengembangan system EWS yang diberikan oleh pemerintah pusat/donor.",
		},

		{
			number_of_questionnaire: 210,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji sistem dan prosedur peringatan dini Kebakaran lahan dan hutan secara berkala oleh multi stakeholder di daerah anda?",
		},

		{
			number_of_questionnaire: 211,
			fill_in_questionnaire: "Apakah sistem peringatan dini dapat turut meningkatkan kesadaran masyarakat akan bahaya Kebakaran Lahan dan Hutan?",
		},

		{
			number_of_questionnaire: 212,
			fill_in_questionnaire: "Apakah system peringatan ini sudah dapat meningkatkan kesiapsiagaan masyarakat dan dunia usaha dari ancaman Kebakaran lahan dan hutan?",
		},

		{
			number_of_questionnaire: 213,
			fill_in_questionnaire: "Apakah sudah ada inisiatif penyusunan rencana kontijensi untuk bencana erupsi gunungapi di daerah anda? (ditambahkan catatan mengenai petingnya keterlibatan multipihak dalam proses penyusunannya)",
		},

		{
			number_of_questionnaire: 214,
			fill_in_questionnaire: "Apakah rencana kontingensi yang disusun telah disahkan dan tersinkronisasi dengan Prosedur Tetap Peringatan Dini dan Penanganan Darurat Bencana Erupsi Gunungapi?",
		},

		{
			number_of_questionnaire: 215,
			fill_in_questionnaire: "Apakah Rencana Kontingensi yang disusun telah diujicoba, dievaluasi, dan mampu dijalankan pada masa krisis dan diturunkan menjadi Rencana Operasi pada masa tanggap darurat bencana Erupsi Gunungapi?",
		},

		{
			number_of_questionnaire: 216,
			fill_in_questionnaire: "Apakah Rencana Kontijensi erupsi gunung api ini telah mempengaruhi kebijakan anggaran di daerah Anda?",
		},

		{
			number_of_questionnaire: 217,
			fill_in_questionnaire: "Apakah ada sudah ada inisiatif untuk membangun sistem peringatan dini erupsi gunungapi di daerah Anda? Catatan: Terdapat peran aktif dari pemerintah daerah dalam inisiasi dan pengembangan system EWS yang diberikan oleh pemerintah pusat/donor.",
		},

		{
			number_of_questionnaire: 218,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji sistem dan prosedur peringatan dini bencana erupsi gunungapi secara berkala oleh multi stakeholder di daerah anda?",
		},

		{
			number_of_questionnaire: 219,
			fill_in_questionnaire: "Apakah sistem peringatan dini dapat turut meningkatkan kesadaran masyarakat akan bahaya Erupsi Gunung Api?",
		},

		{
			number_of_questionnaire: 220,
			fill_in_questionnaire: "Apakah system peringatan ini sudah dapat meningkatkan kesiapsiagaan masyarakat dan dunia usaha dari ancaman erupsi gunungapi?",
		},

		{
			number_of_questionnaire: 221,
			fill_in_questionnaire: "Apakah daerah Anda telah memiliki infrastruktur evakuasi,setidaknya inisiatif pembangunan infrastruktur evakuasi yang dilengkapi dengan rencana evakuasi untuk bencana erupsi gunung api yang disusun berdasarkan pengkajian risiko bencana erupsi gunungapi?",
		},

		{
			number_of_questionnaire: 222,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji untuk sistem evakuasi bencana erupsi gunungapi secara berkala oleh multi stakeholder?",
		},

		{
			number_of_questionnaire: 223,
			fill_in_questionnaire: "Apakah masyarakat telah memahami sistem dan infrastruktur evakuasi gunungapi dengan baik sehingga bisa diterapkan jika bencana erupsi gunungapi terjadi?",
		},

		{
			number_of_questionnaire: 224,
			fill_in_questionnaire: "Apakah masyarakat telah merasakan manfaat dengan adanya rambu peringatan dan/atau rambu evakuasi bencana erupsi gunungapi di daerah anda?",
		},

		{
			number_of_questionnaire: 225,
			fill_in_questionnaire: "Apakah sudah ada inisiatif penyusunan rencana kontijensi untuk bencana kekeringan di daerah anda? (ditambahkan catatan mengenai petingnya keterlibatan multipihak dalam proses penyusunannya)",
		},

		{
			number_of_questionnaire: 226,
			fill_in_questionnaire: "Apakah rencana kontingensi yang disusun telah disahkan dan tersinkronisasi dengan Prosedur Tetap Peringatan Dini dan Penanganan Darurat Bencana kekeringan?",
		},

		{
			number_of_questionnaire: 227,
			fill_in_questionnaire: "Apakah Rencana Kontingensi yang disusun telah diujicoba, dievaluasi, dan mampu dijalankan pada masa krisis dan diturunkan menjadi Rencana Operasi pada masa tanggap darurat bencana kekeringan?",
		},

		{
			number_of_questionnaire: 228,
			fill_in_questionnaire: "Apakah Rencana Kontijensi kekeringan ini dapat mempengaruhi kebijakan anggaran di daerah Anda?",
		},

		{
			number_of_questionnaire: 229,
			fill_in_questionnaire: "Apakah ada sudah ada inisiatif untuk membangun sistem peringatan dini kekeringandi daerah Anda? Catatan: Terdapat peran aktif dari pemerintah daerah dalam inisiasi dan pengembangan system EWS yang diberikan oleh pemerintah pusat/donor.",
		},

		{
			number_of_questionnaire: 230,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji sistem dan prosedur peringatan dini kekeringan secara berkala oleh multi stakeholder di daerah anda?",
		},

		{
			number_of_questionnaire: 231,
			fill_in_questionnaire: "Apakah sistem peringatan dini dapat memingkatkan kesadaran masyarakat akan bahaya Kekeringan?",
		},

		{
			number_of_questionnaire: 232,
			fill_in_questionnaire: "Apakah sistem peringatan dini ini sudah dapat menimbulkan rasa aman masyarakat dari ancaman Kekeringan.",
		},
		{
			number_of_questionnaire: 233,
			fill_in_questionnaire: "Apakah sudah ada inisiatif penyusunan rencana kontijensi untuk bencana banjir bandang di daerah anda? (ditambahkan catatan mengenai petingnya keterlibatan multipihak dalam proses penyusunannya)",
		},

		{
			number_of_questionnaire: 234,
			fill_in_questionnaire: "Apakah rencana kontingensi yang disusun telah disahkan dan tersinkronisasi dengan Prosedur Tetap Peringatan Dini dan Penanganan Darurat Bencana banjir bandang?",
		},

		{
			number_of_questionnaire: 235,
			fill_in_questionnaire: "Apakah Rencana Kontingensi yang disusun telah diujicoba, dievaluasi, dan terbukti mampu dijalankan pada masa krisis dan diturunkan menjadi Rencana Operasi pada masa tanggap darurat bencana banjir bandang?",
		},

		{
			number_of_questionnaire: 236,
			fill_in_questionnaire: "Apakah Rencana Kontijensi Banjir Bandang ini telah mempengaruhi kebijakan anggaran di daerah Anda?",
		},

		{
			number_of_questionnaire: 237,
			fill_in_questionnaire: "Apakah ada sudah ada inisiatif untuk membangun sistem peringatan dini banjir bandang di daerah Anda? Catatan: Terdapat peran aktif dari pemerintah daerah dalam inisiasi dan pengembangan system EWS yang diberikan oleh pemerintah pusat/donor.",
		},

		{
			number_of_questionnaire: 238,
			fill_in_questionnaire: "Apakah telah dilaksanakan pelatihan, simulasi dan uji sistem dan prosedur peringatan dini banjir bandang secara berkala oleh multi stakeholder di daerah anda?",
		},

		{
			number_of_questionnaire: 239,
			fill_in_questionnaire: "Apakah sistem peringatan dini dapat turut meningkatkan kesadaran masyarakat akan bahaya Banjir Bandang?",
		},

		{
			number_of_questionnaire: 240,
			fill_in_questionnaire: "Apakah sistem peringatan dini ini sudah dapat menimbulkan rasa aman masyarakat dari ancaman Banjir Bandang.",
		},

		{
			number_of_questionnaire: 241,
			fill_in_questionnaire: "Apakah telah ada mekanisme prosedur yang mengatur tentang penentuan status darurat bencana dan penggunaan anggaran khusus untuk penanganan darurat bencana di daerah anda?",
		},

		{
			number_of_questionnaire: 242,
			fill_in_questionnaire: "Apakah telah ada mekanisme prosedur yang mengatur tentang penentuan status darurat bencana dan penggunaan anggaran khusus untuk penanganan darurat bencana di daerah anda?",
		},

		{
			number_of_questionnaire: 243,
			fill_in_questionnaire: "Apakah mekanisme penentuan status tanggap darurat tersebut dapat menggerakan masyarakat untuk melakukan tindakan kesiapsiagaan dan penanganan darurat bencana selanjutnya?",
		},

		{
			number_of_questionnaire: 244,
			fill_in_questionnaire: "Apakah penentuan status tanggap darurat tersebut mempengaruhi kebijakan penganggaran OPD (diluar BPBD) terkait penanggulangan bencana?",
		},

		{
			number_of_questionnaire: 245,
			fill_in_questionnaire: "Apakah telah ada mekanisme prosedur yang mengatur tentang struktur komando tanggap darurat bencana di daerah anda?",
		},

		{
			number_of_questionnaire: 246,
			fill_in_questionnaire: "Apakah mekanisme dan prosedur tersebut telah diperkuat dalam sebuah aturan tertulis tentang sistem komando tanggap darurat di daerah anda?",
		},

		{
			number_of_questionnaire: 447,
			fill_in_questionnaire: "Apakah sistem komando tanggap darurat tersebut dipahami oleh seluruh OPD sebagai acuan dalam operasi darurat di kemudian hari?",
		},

		{
			number_of_questionnaire: 248,
			fill_in_questionnaire: "Apakah sistem dan prosedur sistem komando tanggap darurat bencana tersebut dirasakan efektif oleh para pemangku kepentingan dalam situasi tanggap darurat bencana?",
		},

		{
			number_of_questionnaire: 249,
			fill_in_questionnaire: "Apakah telah ada relawan dan personil terlatih yang melakukan kaji cepat pada masa krisis?",
		},

		{
			number_of_questionnaire: 250,
			fill_in_questionnaire: "Apakah telah ada prosedur pengerahan tim dan pelaksanaan kaji cepat pada masa krisis?",
		},

		{
			number_of_questionnaire: 251,
			fill_in_questionnaire: "Apakah relawan dan personil terlatih tersebut melakukan kaji cepat sesuai dengan prosedur yang berlaku?",
		},

		{
			number_of_questionnaire: 252,
			fill_in_questionnaire: "Apakah hasil kaji cepat tersebut dijadikan acuan dalam penentuan status tanggap darurat bencana?",
		},

		{
			number_of_questionnaire: 253,
			fill_in_questionnaire: "Apakah telah adarelawan dan personil terlatih yang melakukan penyelamatan dan pertolongan korban pada masa krisis dan tanggap darurat bencana?",
		},

		{
			number_of_questionnaire: 254,
			fill_in_questionnaire: "Apakah telah ada prosedur pengerahan tim dan pelaksanaan penyelamatan dan pertolongan korban pada masa krisis dan tanggap darurat bencana?",
		},

		{
			number_of_questionnaire: 255,
			fill_in_questionnaire: "Apakah tim penyelamatan dan pertolongan korban terlatih tersebut melakukan tugasnya sesuai dengan prosedur yang berlaku?",
		},

		{
			number_of_questionnaire: 256,
			fill_in_questionnaire: "Apakah tim dan pelaksanaan penyelamatan dan pertolongan korbanmelaksanakan tugasnya secara efektif?",
		},

		{
			number_of_questionnaire: 257,
			fill_in_questionnaire: "Apakah telah ada prosedur perbaikan darurat bencana untuk pemulihan fungsi fasilitas kritis pada masa tanggap darurat bencana?",
		},

		{
			number_of_questionnaire: 258,
			fill_in_questionnaire: "Apakah prosedur tersebut telah diperkuat melalui sebuah aturan daerah?",
		},

		{
			number_of_questionnaire: 259,
			fill_in_questionnaire: "Apakah pada prosedur tersebut telah mengakomodir peran pemerintah, komunitas dan dunia usaha, dalam perbaikan darurat?",
		},

		{
			number_of_questionnaire: 260,
			fill_in_questionnaire: "Apakah prosedur perbaikan darurat bencana tersebut dapat memulihkan fungsi fasilitas kritis dengan segera pada masa tanggap darurat?",
		},

		{
			number_of_questionnaire: 261,
			fill_in_questionnaire: "Apakah telah ada relawan dan personil yang melakukan pendistribusian bantuan kemanusiaan bagi masyarakat yang sulit dijangkau pada masa krisis dan tanggap darurat bencana?",
		},

		{
			number_of_questionnaire: 262,
			fill_in_questionnaire: "Apakah telah ada mekanisme dan prosedur untuk penggalangan dan/atau pengerahan bantuan darurat bencana?",
		},

		{
			number_of_questionnaire: 263,
			fill_in_questionnaire: "Apakah relawan dan personil yang melakukan pendistribusian bantuan kemanusiaan melaksanakan tugas sesuai prosedur?",
		},

		{
			number_of_questionnaire: 264,
			fill_in_questionnaire: "Apakah prosedur pendistribusian bantuan kemanusian tersebut mampu menjangkau masyarakat terjauh?",
		},

		{
			number_of_questionnaire: 265,
			fill_in_questionnaire: "Apakah telah ada aturan tertulis tentang prosedur penghentian status tanggap darurat bencana?",
		},

		{
			number_of_questionnaire: 266,
			fill_in_questionnaire: "Apakah prosedur tersebut telah mengatur mekanisme proses transisi/peralihan dari tanggap darurat ke rehabilitasi dan rekonstruksi?",
		},

		{
			number_of_questionnaire: 267,
			fill_in_questionnaire: "Apakah penentuan status tanggap darurat tersebut dipercaya masyarakat sebagai akhir dari masa tanggap darurat?",
		},

		{
			number_of_questionnaire: 268,
			fill_in_questionnaire: "Apakah prosedur penghentian status tanggap darurat mengembalikan kondisi aktivitas masyarakat?",
		},

		{
			number_of_questionnaire: 269,
			fill_in_questionnaire: "Apakah telah ada inisiatif untuk membangun mekanisme dan/atau rencana pemulihan pelayanan dasar pemerintah pasca bencana bagi sebagian ancaman bencana di daerah?",
		},

		{
			number_of_questionnaire: 270,
			fill_in_questionnaire: "Apakah mekanisme dan/atau rencana pemulihan pelayanan dasar pemerintah tersebut telah secara formal disepakati oleh seluruh pemangku kepentingan di daerah?",
		},

		{
			number_of_questionnaire: 271,
			fill_in_questionnaire: "Apakah rancangan tersebut telah mengakomodir seluruh ancaman bencana; kebutuhan dan peran pemerintah, komunitas, dan sektor swasta dalam proses rehabilitasi dan rekonstruksi?",
		},

		{
			number_of_questionnaire: 272,
			fill_in_questionnaire: "Adakah jaminan kelanjutan semua fungsi pemerintahan dan/atau administrasi penting pasca bencana?",
		},

		{
			number_of_questionnaire: 273,
			fill_in_questionnaire: "Apakah telah ada mekanisme dan/atau rencana pemulihan infrastruktur penting pasca bencana?",
		},

		{
			number_of_questionnaire: 274,
			fill_in_questionnaire: "Apakah telah ada mekanisme dan/atau rencana dan pelaksanaan pemulihan infrastruktur penting pasca bencana, yang disusun secara bersama oleh pemangku kepentingan dan mempertimbangkan kebutuhan korban?",
		},

		{
			number_of_questionnaire: 275,
			fill_in_questionnaire: "Apakah rancangan proses - proses pemulihan infrastruktur penting pasca bencana telah disusun dengan mempertimbangkan prinsip-prinsip risiko bencana jangka panjang (slow onset) guna menghindari risiko baru dari pembangunan?",
		},

		{
			number_of_questionnaire: 276,
			fill_in_questionnaire: "Adakah jaminan keberlangsungan fungsi infrastruktur penting pasca bencana di daerah?",
		},

		{
			number_of_questionnaire: 277,
			fill_in_questionnaire: "Apakah telah ada system atau mekanisme daerah untuk perbaikan rumah penduduk pasca bencana? Baik atas dukungan pemerintah maupun swadaya atau pihak lain.",
		},

		{
			number_of_questionnaire: 278,
			fill_in_questionnaire: "Apakah telah ada mekanisme dan/atau rencana dan pelaksanaan perbaikan rumah penduduk pasca bencana yang disusun secara bersama oleh pemangku kepentingan dan mempertimbangkan kebutuhan dasar korban?",
		},

		{
			number_of_questionnaire: 279,
			fill_in_questionnaire: "Apakah rancangan proses - proses perbaikan rumah penduduk pasca bencana disusun telah mempertimbangkan prinsip-prinsip risiko bencana guna menghindari risiko jangka panjang (slow onset) dari pembangunan?",
		},

		{
			number_of_questionnaire: 280,
			fill_in_questionnaire: "Apakah perbaikan rumah penduduk yang telah/sedang dilaksanakan telah mampu secara terukur mengurangi risiko masyarakat terhadap ancaman bencana yang telah terjadi?",
		},

		{
			number_of_questionnaire: 281,
			fill_in_questionnaire: "Apakah telah ada mekanisme dan/atau rencana rehabilitasi dan pemulihan penghidupan masyarakat pasca bencana?",
		},

		{
			number_of_questionnaire: 282,
			fill_in_questionnaire: "Apakah telah ada mekanisme dan/atau rencana dan pelaksanaan pemulihan penghidupan masyarakat pasca bencana yang disusun secara bersama oleh pemangku kepentingan dan mempertimbangkan kebutuhan korban?",
		},

		{
			number_of_questionnaire: 283,
			fill_in_questionnaire: "Apakah pemulihan penghidupan masyarakat pasca bencana yang disusun telah mempertimbangkan prinsip-prinsip risiko bencana jangka panjang (slow onset) guna menghindari risiko baru dari penghidupan masyarakat?",
		},

		{
			number_of_questionnaire: 284,
			fill_in_questionnaire: "Apakah proses pemulihan penghidupan masyarakat pasca bencana telah membangun budaya komunitas yang berorientasi pada aspek kapasitas jaringan pangan, kesehatan umum, perekonomian dalam hal pengurangan terbentuknya kelompok-kelompok miskin dan asuransi infrastruktur dan asset penduduk dengan partisipasi setiap komponen komunitas?",
		},
	]
};

module.exports = DATA;
